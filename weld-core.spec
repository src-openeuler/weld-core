%global namedreltag .Final
%global namedversion %{version}%{?namedreltag}
Name:                weld-core
Version:             2.3.5
Release:             4
Summary:             Reference Implementation for JSR-299: Contexts and Dependency Injection (CDI)
License:             ASL 2.0 and (CDDL-1.0 or GPLv2 with exceptions) and LGPLv2+ and MIT and OFL-1.1
URL:                 http://weld.cdi-spec.org/
Source0:             https://github.com/weld/core/archive/%{namedversion}/%{name}-%{namedversion}.tar.gz
Patch0:              0001-Add-support-for-newer-jboss-logging-tools.patch
Patch1:              weld-core-2.3.2.Final-Remove-gwtdev-environment.patch
BuildArch:           noarch
BuildRequires:       maven-local mvn(com.google.guava:guava:18.0) mvn(io.undertow:undertow-servlet)
BuildRequires:       mvn(javax.el:el-api) mvn(javax.enterprise:cdi-api) mvn(javax.faces:jsf-api)
BuildRequires:       mvn(javax.portlet:portlet-api) mvn(javax.servlet.jsp:jsp-api) mvn(junit:junit)
BuildRequires:       mvn(net.sourceforge.findbugs:annotations)
BuildRequires:       mvn(org.apache.maven.plugins:maven-antrun-plugin)
BuildRequires:       mvn(org.apache.maven.plugins:maven-source-plugin)
BuildRequires:       mvn(org.apache.tomcat:tomcat-catalina) mvn(org.codehaus.groovy:groovy-all)
BuildRequires:       mvn(org.eclipse.jetty:jetty-plus)
BuildRequires:       mvn(org.hibernate.javax.persistence:hibernate-jpa-2.1-api)
BuildRequires:       mvn(org.jboss.arquillian:arquillian-bom:pom:)
BuildRequires:       mvn(org.jboss.classfilewriter:jboss-classfilewriter) mvn(org.jboss:jandex)
BuildRequires:       mvn(org.jboss.jandex:jandex-maven-plugin) mvn(org.jboss.logging:jboss-logging)
BuildRequires:       mvn(org.jboss.jdeparser:jdeparser:1)
BuildRequires:       mvn(org.jboss.logging:jboss-logging-processor:1)
BuildRequires:       mvn(org.jboss.shrinkwrap:shrinkwrap-impl-base)
BuildRequires:       mvn(org.jboss.spec.javax.annotation:jboss-annotations-api_1.2_spec)
BuildRequires:       mvn(org.jboss.spec.javax.ejb:jboss-ejb-api_3.2_spec)
BuildRequires:       mvn(org.jboss.spec.javax.el:jboss-el-api_3.0_spec)
BuildRequires:       mvn(org.jboss.spec.javax.faces:jboss-jsf-api_2.2_spec)
BuildRequires:       mvn(org.jboss.spec.javax.interceptor:jboss-interceptors-api_1.2_spec)
BuildRequires:       mvn(org.jboss.spec.javax.servlet:jboss-servlet-api_3.1_spec)
BuildRequires:       mvn(org.jboss.spec.javax.transaction:jboss-transaction-api_1.2_spec)
BuildRequires:       mvn(org.jboss.weld:weld-api) mvn(org.jboss.weld:weld-parent:pom:)
BuildRequires:       mvn(org.jboss.weld:weld-spi)
BuildRequires:       java-11-openjdk-devel
Requires:            java-11-openjdk
Requires:            javapackages-tools
Provides:            bundled(fontawesome-fonts) = 4.2.0
Provides:            bundled(fontawesome-fonts-web) = 4.2.0
%description
Weld is the reference implementation (RI) for JSR-299: Java Contexts and
Dependency Injection for the Java EE platform (CDI). CDI is the Java standard
for dependency injection and contextual lifecycle management, and integrates
cleanly with the Java EE platform. Any Java EE 6-compliant application server
provides support for JSR-299 (even the web profile).

%package javadoc
Summary:             Javadoc for %{name}
%description javadoc
This package contains the API documentation for %{name}.

%prep
%setup -q -n core-%{namedversion}
%patch0 -p1
%patch1 -p1
find . -name '*.jar' -exec rm {} \;
find . -name '*.class' -exec rm {} \;
%pom_remove_plugin org.eclipse.m2e:lifecycle-mapping
%pom_disable_module porting-package
%pom_disable_module tests
%pom_disable_module tests-arquillian
%pom_disable_module inject-tck-runner
%pom_disable_module jboss-tck-runner
%pom_disable_module tests/base environments/servlet
%pom_disable_module tests/jetty environments/servlet
%pom_disable_module tests/tomcat environments/servlet
%pom_disable_module tests environments/se
%pom_disable_module tests probe
%pom_remove_plugin ro.isdc.wro4j:wro4j-maven-plugin probe/core
%pom_remove_plugin org.apache.maven.plugins:maven-checkstyle-plugin
rm -rf environments/servlet/core/src/main/java/org/jboss/weld/environment/gwtdev
%pom_remove_dep -r org.mortbay.jetty environments/servlet
%pom_remove_plugin org.apache.maven.plugins:maven-compiler-plugin environments/se/core
%pom_remove_plugin -r :maven-shade-plugin
%pom_remove_plugin -r :maven-shade-plugin environments/se/build environments/servlet/build
%pom_xpath_remove -r pom:dependency/pom:optional environments/se/build environments/servlet/build
%pom_change_dep org.glassfish:javax.el javax.el:el-api tests-common
%pom_xpath_set pom:properties/pom:jboss.logging.processor.version 1
%pom_change_dep -r :jboss-logging-processor ::1
sed -i -e 's/InstantiationException/InstantiationException, NoSuchMethodException/' \
  environments/servlet/core/src/main/java/org/jboss/weld/environment/tomcat/{Weld,}ForwardingInstanceManager.java

%build
export JAVA_HOME=%{_jvmdir}/java-11-openjdk
export CFLAGS="${RPM_OPT_FLAGS}"
export CXXFLAGS="${RPM_OPT_FLAGS}"
%mvn_build -f

%install
%mvn_install

%files -f .mfiles
%doc README.md

%files javadoc -f .mfiles-javadoc

%changelog
* Mon Aug 21 2023 yaoxin <yao_xin001@hoperun.com> - 2.3.5-4
- Fix build failure caused by jboss-classfilewriter upgrade to 1.3.0

* Mon Aug 8 2022 Chenyx <chenyixiong3@huawei.com> - 2.3.5-3
- License compliance rectification

* Thu Dec 03 2020 huanghaitao <huanghaitao8@huawei.com> - 2.3.5-2
- Remove dependence with jetty8

* Mon Aug 17 2020 Shaoqiang Kang <kangshaoqiang1@huawei.com> - 2.3.5-1
- Package init
